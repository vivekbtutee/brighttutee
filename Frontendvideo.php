<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);
class Frontendvideo extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Frontend_model', 'User', 'User_model', 'Student_model'));
		$this->load->library('pagination'); 
		$this->perPage = 6;
	}
   public function freeVideos()
    {
		
        $boardid = array();
        $classid = array();
        $mediumid = array();
		$conditions = array();

        $get_board = $this->input->get('board');
        $data['board_select'] = array();
        if (!empty($get_board)) {
            $data['board_select'] = $get_board;
            foreach ($data['board_select'] as $board) {

                $this->db->select('id');
                $this->db->from('board');
                $this->db->where('board', $board);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    $res = $query->result();
                    $board_id = $res[0]->id;
                    array_push($boardid, $board_id);
                }
            }
        }

        // Get Class
        $get_class = $this->input->get('class');
        $data['class_select'] = array();
        if (!empty($get_class)) {
            $data['class_select'] = $get_class;
            foreach ($data['class_select'] as $class) {
                $this->db->select('id');
                $this->db->from('class');
                $this->db->where('class', $class);
                $query1 = $this->db->get();
                if ($query1->num_rows() > 0) {
                    $result = $query1->result();
                    $class_id = $result[0]->id;
                    array_push($classid, $class_id);
                }
            }
        }

        // Get medium
        $get_medium = $this->input->get('medium');
        $data['medium_select'] = array();
        if (!empty($get_medium)) {
            $data['medium_select'] = $get_medium;
            foreach ($data['medium_select'] as $medium) {
                $this->db->select('id');
                $this->db->from('language');
                $this->db->where('language', $medium);
                $query1 = $this->db->get();
                if ($query1->num_rows() > 0) {
                    $result = $query1->result();
                    $medium_id = $result[0]->id;
                    array_push($mediumid, $medium_id);
                }
            }
        }

        $data['board'] = false;
        $data['class'] = false;

        $table = "board";
        $orderby = "board";
        $sortby = "asc";
        $data['boards'] = $this->Frontend_model->getdata($table, $orderby, $sortby);

        // $product_type = 'course';
        // $data['boardcounts'] = $this->Frontend_model->getboardcount($product_type);

        $table = "class";
        $orderby = "sortorder";
        $sortby = "asc";
        $data['classes'] = $this->Frontend_model->getdata($table, $orderby, $sortby);

        $table = "language";
        $orderby = "id";
        $sortby = "asc";
        $data['languages'] = $this->Frontend_model->getdata($table, $orderby, $sortby);


        $table1 = "freevideos";
        $find = "url";
        $product_type = "course";
        $title = $this->input->get('title');

        $board = '';

        if (!empty($title)) {
            $title = trim($this->input->get('title'));
        }

        $allboardid = implode(',', $boardid);
        $allclassid = implode(',', $classid);
        $allmediumid = implode(',', $mediumid);
		$data['search_title'] = $title;
		$baseurl = base_url() . $this->router->class . '/' . $this->router->method . "/" . $title;
		$allrecord = $this->Frontend_model->record_count_free_video($table1, $find, $product_type, $title, $allboardid, $allclassid, $allmediumid);
        //print_r($allrecord);exit;
        $data["products"] = $this->Frontend_model->fetch_free_video($table1, $find, $product_type, $title, $allboardid, $allclassid, $allmediumid, array(''));
		
		//START Pagination Code
		$conditions['returnType'] = 'count';
		$config['base_url']    = $baseurl;
		$totalRec = count($data["products"]);
		$uriSegment = 3; 
		$config['uri_segment'] = $uriSegment; 
        $config['total_rows']  = $totalRec; 
        $config['per_page']    = $this->perPage; 
		
		$config['num_tag_open'] = '<li>'; 
		$config['num_tag_close'] = '</li>'; 
		$config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">'; 
		$config['cur_tag_close'] = '</a></li>'; 
		$config['next_link'] = 'Next'; 
		$config['prev_link'] = 'Prev'; 
		$config['next_tag_open'] = '<li class="pg-next">'; 
		$config['next_tag_close'] = '</li>'; 
		$config['prev_tag_open'] = '<li class="pg-prev">'; 
		$config['prev_tag_close'] = '</li>'; 
		$config['first_tag_open'] = '<li>'; 
		$config['first_tag_close'] = '</li>'; 
		$config['last_tag_open'] = '<li>'; 
		$config['last_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		$page = $this->uri->segment($uriSegment); 
        $offset = !$page?0:$page; 
         
        // Get records 
        $conditions = array( 
            'start' => $offset, 
            'limit' => $this->perPage 
        );
		$data["products"] = $this->Frontend_model->fetch_free_video($table1, $find, $product_type, $title, $allboardid, $allclassid, $allmediumid, $conditions);
		//END Pagination Code
		
		
		$data['title'] = 'Free Online Classes Videos for CBSE Class 9 & 10 | Bright Tutee ';
        $data['description'] = 'Get free online demo classes videos for CBSE Class 9 & 10 - Science (Physics & Chemistry), Social Science, Maths, Biology, Hindi & English. Call on 99-7137-9962.';

        $data['keywords'] = 'free online coaching for class 9 cbse, free online classes for class 12 cbse,free online tuition for class 9, free online coaching class 10, free online classes videos for cbse class 10';

	
     	$this->load->view('video', $data);
        /*$this->load->view('common/footer');
        $this->load->view('common/js');*/
    }
	
	
	
	
	public function live_lecture_detail()
    {
		
		
		/************************** mediumg ********/
		 $get_medium = $this->input->get('medium');
        $data['medium_select'] = array();
        if (!empty($get_medium)) {
            $data['medium_select'] = $get_medium;
            foreach ($data['medium_select'] as $medium) {
                $this->db->select('id');
                $this->db->from('language');
                $this->db->where('language', $medium);
                $query1 = $this->db->get();
                if ($query1->num_rows() > 0) {
                    $result = $query1->result();
                    $medium_id = $result[0]->id;
                    array_push($mediumid, $medium_id);
                }
            }
        }
		
		
		
	$allclassid = implode(',', $classid);
    $allmediumid = implode(',', $mediumid);
	$data['title'] = 'Online Live lecture';
	$data['keyword'] = '';
	$data['description'] = '';
	$data["products"] = $this->Frontend_model->fetch_live_lecture($allmediumid, $allclassid);
    $this->load->view('video_lecture', $data);
    
    }
}
	

	?>
